import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from './pages/Login'
import Principal from './pages/Principal'
import BarCodeScannerPage from './pages/BarCodeScanner'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function Root() {
    return (
        <Drawer.Navigator
            drawerType='back'>
            <Drawer.Screen name="principal" component={Principal} />
            <Drawer.Screen name="barcode" component={BarCodeScannerPage} />
        </Drawer.Navigator>
    )
}

export default function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Root" component={Root} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}