import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import Constants from 'expo-constants';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end'
  },
  imageBackground: {
    flex: 1,
    height: null,
    width: null
  },


  imageLogo: {
    height: height * 0.1,
    width: width * 0.6,
    marginHorizontal: width * 0.2,
    marginHorizontal: height * 0.1,
  },

  loginContainer: {
    backgroundColor: 'white',
    height: height / 2,
    marginVertical: 0,
    marginHorizontal: width * 0.1,
  },

  forgotPassword: {
    color: '#005B78'
  }

  // buttonsContainer: {
  //   height: height / 3,
  //   justifyContent: 'center'
  // },
  // button: {
  //   backgroundColor: 'white',
  //   height: 70,
  //   marginHorizontal: 20,
  //   borderRadius: 35,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   marginVertical: 5
  // },
  // buttonSignin: {
  //   fontSize: 20,
  //   fontWeight: 'bold'
  // },
});