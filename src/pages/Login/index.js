import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';
import { TextInput, Button, Text } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';

export default function Login() {

    const navigation = useNavigation();
    function navigateToDetail() {
        navigation.navigate('Root', { screen: 'principal' });
    }

    const state = {
        text: ''
    };

    return (
        <View style={styles.container}>
            {/* Image Background */}
            <Image
                source={require('../../assets/bg.jpg')}
                style={styles.imageBackground}
            />

            <Image
                source={require('../../assets/transparent_logo.png')}
                style={styles.imageLogo}
            />

            {/* Login Container */}
            <View style={styles.loginContainer}>
                <TextInput
                    label='USUÁRIO'
                    value={state.text}
                    onChangeText={text => this.setState({ text })}
                />
                <TextInput
                    label='SENHA'
                    value={state.text}
                    onChangeText={text => this.setState({ text })}
                />
                <Text style={styles.forgotPassword}>Esqueceu a senha?</Text>
                <Button
                    loading="false"
                    mode="contained" onPress={() => navigateToDetail()}>
                    ENTRAR
                    </Button>
            </View>
        </View>
    )

}