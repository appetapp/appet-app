import React, { Component } from 'react';
import { View, FlatList, Dimensions, Text, Image, TouchableHighlight } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as StoreProductActions from '../../store/actions/storeProduct';

import styles from './styles';
import Header from '../../components/Header';
import Carousel from 'react-native-snap-carousel';

import { scrollInterpolator, animatedStyles } from '../../utils/animations';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

class Principal extends Component {
    constructor(props) {
        super(props);
        props.fetchingData()
    }
    _renderItem = ({ item, index }) => {
        console.log("item", item)
        return (
            <View style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',

                backgroundColor: '#fff',
                shadowColor: "#000",
                shadowOffset: {
                    width: 1,
                    height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
            }}>
                <Image
                    style={styles.productImage} source={{ uri: item.product.imageUrl }} />
                <View style={styles.insideCarroselItem}>
                    <Text>{item.product.name}</Text>
                    <Text style={{ fontWeight: 'bold' }}>R$ {item.price}</Text>
                </View>
            </View>
        );
    }

    render() {
        const { classes, storeProduct, user, history } = this.props;

        console.log("storeProduct", storeProduct)

        return (
            <View>
                <Header />
                <Text style={styles.pageHeader}>Veja o que pode encontrar em nossas PetParceiras:</Text>
                {/* <Loading /> */}
                {
                    storeProduct.length == 0 ?
                        <Text>Não foi possível carregar dados do servidor</Text> :
                        <FlatList
                            data={storeProduct}
                            style={styles.incidentList}
                            showsHorizontalScrollIndicator='false'
                            renderItem={({ item }) =>
                                <View key={item.id} style={styles.petShopItem}>
                                    <View style={styles.petShopListHeader}>
                                        <View style={styles.petShopLogoContainer}>
                                            <Image style={styles.petShopLogo} source={{ uri: item.logoUrl }} />
                                        </View>
                                        <View>
                                            <Text style={styles.petShopListHeaderTitle}>{item.name}</Text>
                                            <Text>{`${item.address.street}, ${item.address.number} - ${item.address.neighborhood}`}</Text>
                                        </View>
                                    </View>
                                    <Carousel
                                        ref={(c) => { this._carousel = c; }}
                                        layout={'default'}
                                        data={item.productList}
                                        sliderWidth={SLIDER_WIDTH}
                                        itemWidth={ITEM_WIDTH}
                                        renderItem={this._renderItem}
                                        useScrollView={true}
                                    />
                                </View>

                            }
                            keyExtractor={petshop => petshop.realName} />
                }
            </View>
        )
    }
}

const mapStateToProps = ({ storeProduct, user }) => ({
    storeProduct,
    user
})


const mapDispatchToProps = dispatch =>
    bindActionCreators(StoreProductActions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Principal)