import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import Constants from 'expo-constants';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    productImage: {
        width: width / 3,
        height: height / 4,
    },
    pageHeader: {
        fontSize: 25,
        marginLeft: width * 0.03,
        marginTop: width * 0.03
    },
    petShopItem: {
        marginTop: width * 0.04,
        marginLeft: width * 0.04,
        marginRight: width * 0.04,
        padding: width * 0.03,
        backgroundColor: "#fff",
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    insideCarroselItem: {
        marginTop: 20
    },
    petShopListHeader: {
        display: 'flex',
        flexDirection: 'row',
        width: width * 0.6,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    petShopListHeaderTitle: {
        fontWeight: "bold",
        fontSize: 18
    },
    petShopLogoContainer: {
        width: width / 5,
        marginRight: width * 0.02,
        height: height / 10,
    },
    petShopLogo: {
        width: width / 8,
        height: height / 10,
    },
    cardContent: {
        backgroundColor: '#fff',
    }
});