import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import Constants from 'expo-constants';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    loadingComponent: {
        flex: 1, //ocupa tamanho todo
        paddingHorizontal: 24, //igual a 0 24px
        paddingTop: Constants.statusBarHeight + 20, //ob
    }
});