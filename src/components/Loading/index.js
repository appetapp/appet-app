import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-paper';

import styles from './styles';


export default function Loading() {
    return (
        <View style={styles.loadingComponent}>
            <Text>oi22</Text>
            <Button loading="true"
                icon="camera"
                mode="contained"
                onPress={() => console.log('Pressed')}>
                Press me
            </Button>
        </View>
    )

}