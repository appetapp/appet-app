import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';
import { TextInput, Button, Text } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import logoImg from '../../assets/logo.png';


export default function Header() {

    const navigation = useNavigation();

    return (
        <View style={styles.header}>
            <Button color="white" icon="menu" onPress={() => navigation.toggleDrawer()} />
            <Image style={styles.headerLogo} source={logoImg} />
        </View>
    )

}