import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import Constants from 'expo-constants';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    header: {
        paddingTop: Constants.statusBarHeight,
        height: height * 0.12,
        flexDirection: 'row',
        alignItems: "center",
        backgroundColor: '#005B78',
    },
    headerLogo:{
        width: width * 0.25,
        height: height * 0.04,
        marginLeft: width * 0.21
    },
    headerText: {
        fontSize: 15,
        color: '#737380'
    },
    headerTextBold: {
        fontWeight: 'bold'
    },
});